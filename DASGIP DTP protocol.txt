
Device addres 			1b		0 - 32, 255 broadcast(?)
Message lenght		2b 		Total message array size (size incl. checksum)
Command			1b		Read:48, Write:49
Array type(?)			2b		SEND U16:1234 -> U8:210;4
Number of registers		2b		Total number of registers to read or write
Registers{						See (1)
	Register index		2b
	Register value		4b
}
Checksum			2b		Calculated checksum



(1) Registers index
nnmm
MP8 pump module:
nn 		pump address 		14 - 22 (13+pumpnumber)
mm 		register
	7 - pump mode (4 = N.SPR (#.SetPoint Remote) unit: 1/min)
	34 - pumpspeed (0 - 100.000 N.SPR (#.SetPoint Remote) unit: 1/min)
	13 - runstate (off/on = 1/2)
	32 - pump totalizer calibration (rotations/ml) RW
	22 - volume totalizer RW